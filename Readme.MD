# TsToXML
  
Easy way to extract the xml TaskSequence  
  
## Getting Started  
 
[TsToXml](https://www.dropbox.com/s/jkitub6exub1vbk/TsToXml.zip?dl=0)
.Net Framework 4.5.1  
  
Open Exe with a ConfigMgr account that can read the task sequence.  
Fill the Primary Site FQDN.  
Click Connect.  
Select The TaskSequence you want.  
Create XML.  
Save File.  


## Authors
  
* **Morin Ludovic** - *Initial work* 
