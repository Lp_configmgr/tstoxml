﻿using System;
using System.Windows;
using System.Reflection;
using Microsoft.Win32;
using System.IO;
using System.Xml.Linq;
using EzConfigMgr;
using System.Collections;
using Microsoft.ConfigurationManagement.ManagementProvider;
using System.Collections.Generic;
using System.Management;
using System.Windows.Input;

namespace TsToXml
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public Primary cmPrimary;
        public string lastElement = "group";
        public object oEndOfDoc = "\\endofdoc"; /* \endofdoc is a predefined bookmark */

        public MainWindow()
        {
            InitializeComponent();
            DataContext = this;
        }


        private void Create_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog box = new SaveFileDialog();
            box.Filter = "xml files (*.xml)|*.xml";
            if( box.ShowDialog() == true)
            {                 
                ((TaskSequence)CbTS.SelectedItem).Sequence.Save(box.FileName);
                this.Close();
            }
        }

        //Save User Settings when software close
        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            Properties.Settings.Default.Save();
            base.OnClosing(e);
        }

        private void Connect_Click(object sender, RoutedEventArgs e)
        {

            cmPrimary = new Primary(TBPrimary.Text);
            try
            {
                IResultObject tempList = cmPrimary.GetTsList();
                List<TaskSequence> tsList = new List<TaskSequence>();

                foreach (IResultObject result in tempList)
                {
                    result.Get();
                    String strSequence = result["Sequence"].StringValue;
                    String packageID = result["PackageID"].StringValue;
                    String name = result["Name"].StringValue;
                    XElement sequence = XElement.Parse(strSequence);
                    TaskSequence ts = new TaskSequence(name, sequence);
                    tsList.Add(ts);
                }
                CbTS.ItemsSource = tsList;
                CbTS.UpdateLayout();
                CbTS.IsEnabled = true;
            }
            catch(Exception exc)
            {
                Info exception = new Info(exc.Message, 3);
                exception.ShowDialog();
                Environment.Exit(1);
            }
        }

        private void CbTS_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            Create.IsEnabled = true;
        }

        private Version GetRunningVersion()
        {
            return Assembly.GetExecutingAssembly().GetName().Version;
        }

        private void Title_Drag(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left) { DragMove(); }
        }

        private void Close_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Minimize_Click(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState.Minimized;
        }
    }
}